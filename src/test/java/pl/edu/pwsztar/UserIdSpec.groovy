package pl.edu.pwsztar

import spock.lang.Specification

class UserIdSpec extends Specification {
    def randomIdListToTests = ['87541254877', '12451278546', '01231789631', '97060106252', '86010184611', '05261551601', 'zaqwsxzaqws', 'xxx']

    def "should check correct size"() {
        given:
        UserId userId = new UserId(randomIdListToTests.get(0))
        UserId userId1 = new UserId(randomIdListToTests.get(1))
        UserId userId2 = new UserId(randomIdListToTests.get(2))
        UserId userId3 = new UserId(randomIdListToTests.get(3))
        UserId userId4 = new UserId(randomIdListToTests.get(4))
        UserId userId5 = new UserId(randomIdListToTests.get(5))
        UserId userId6 = new UserId(randomIdListToTests.get(6))
        UserId userId7 = new UserId(randomIdListToTests.get(7))
        when:
        def answer = [userId.isCorrectSize(),
                      userId1.isCorrectSize(),
                      userId2.isCorrectSize(),
                      userId3.isCorrectSize(),
                      userId4.isCorrectSize(),
                      userId5.isCorrectSize(),
                      userId6.isCorrectSize(),
                      userId7.isCorrectSize(),]
        then:
        answer == [true,true,true,true,true,true,true,false]
    }
    def "should check sex is correct insert"(){
        given:
        UserId userId = new UserId(randomIdListToTests.get(0))
        UserId userId1 = new UserId(randomIdListToTests.get(1))
        UserId userId2 = new UserId(randomIdListToTests.get(2))
        UserId userId3 = new UserId(randomIdListToTests.get(3))
        UserId userId4 = new UserId(randomIdListToTests.get(4))
        UserId userId5 = new UserId(randomIdListToTests.get(5))
        UserId userId6 = new UserId(randomIdListToTests.get(6))
        UserId userId7 = new UserId(randomIdListToTests.get(7))

        when:
        def answer = [(userId.getSex().toString() == "Optional[WOMAN]"),
                      userId1.getSex().toString() == "Optional[MAN]",
                      userId2.getSex().toString() == "Optional[MAN]",
                      userId3.getSex().toString() == "Optional[MAN]",
                      userId4.getSex().toString() == "Optional[MAN]",
                      userId5.getSex().toString() == "Optional[WOMAN]",
                      userId6.getSex().toString() == "Optional.empty",
                      userId7.getSex().toString() == "Optional.empty"]
        then:
        answer == [false,false,false,true,true,true,true,true]
    }

    def "should check if id is correct with the rules"(){
        given:
        UserId userId = new UserId(randomIdListToTests.get(0))
        UserId userId1 = new UserId(randomIdListToTests.get(1))
        UserId userId2 = new UserId(randomIdListToTests.get(2))
        UserId userId3 = new UserId(randomIdListToTests.get(3))
        UserId userId4 = new UserId(randomIdListToTests.get(4))
        UserId userId5 = new UserId(randomIdListToTests.get(5))
        UserId userId6 = new UserId(randomIdListToTests.get(6))
        UserId userId7 = new UserId(randomIdListToTests.get(7))
        when:
        def answer = [userId.isCorrect(),
                      userId1.isCorrect(),
                      userId2.isCorrect(),
                      userId3.isCorrect(),
                      userId4.isCorrect(),
                      userId5.isCorrect(),
                      userId6.isCorrect(),
                      userId7.isCorrect()]
        then:
        answer == [false,false,false,true,true,true,false,false]
    }
    def "should if return date is correct with pattern :  (dd-mm-yyyy)"(){
        given:
        UserId userId = new UserId(randomIdListToTests.get(0))
        UserId userId1 = new UserId(randomIdListToTests.get(1))
        UserId userId2 = new UserId(randomIdListToTests.get(2))
        UserId userId3 = new UserId(randomIdListToTests.get(3))
        UserId userId4 = new UserId(randomIdListToTests.get(4))
        UserId userId5 = new UserId(randomIdListToTests.get(5))
        UserId userId6 = new UserId(randomIdListToTests.get(6))
        UserId userId7 = new UserId(randomIdListToTests.get(7))
        when:
        def answer = [(userId.getDate() == Optional.of('01-01-1990')),
                      userId1.getDate() == Optional.of('01-01-1990'),
                      userId2.getDate() == Optional.of('01-01-1990'),
                      userId3.getDate() == Optional.of('01-06-1997'),
                      userId4.getDate() == Optional.of('01-01-1986'),
                      userId5.getDate() == Optional.of('15-06-2005'),
                      userId6.getDate() == Optional.empty(),
                      userId7.getDate() == Optional.empty()]
        then:
        answer == [false,false,false,true,true,true,true,true]
    }

}
